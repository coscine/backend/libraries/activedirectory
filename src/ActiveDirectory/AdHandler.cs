﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using System;
using System.DirectoryServices;

namespace Coscine.ActiveDirectory
{
    public class ADHandler
    {

        private static DirectoryEntry GetCoscineEntry(IConfiguration configuration)
        {
            // If keys exist
            if (configuration.KeysAndWait("coscine/local/profilesync") != null)
            {
                var adDomain = configuration.GetStringAndWait("coscine/local/profilesync/domain");
                var adUsername = adDomain + @"\" + configuration.GetStringAndWait("coscine/local/profilesync/username");
                var adPassword = configuration.GetStringAndWait("coscine/local/profilesync/password");

                #pragma warning disable CA1416 // Validate platform compatibility
                using (DirectoryEntry directoryEntry = new DirectoryEntry("LDAP://" + configuration.GetStringAndWait("coscine/local/profilesync/forestname"), adUsername, adPassword))
                {
                    var ou = configuration.GetStringAndWait("coscine/local/profilesync/ou");
                    if (ou.Contains(",DC"))
                    {
                        ou = ou.Substring(0, ou.IndexOf(",DC"));
                    }
                    return directoryEntry.Children.Find(ou);
                }
            }
            else
            {
                return null;
            }

        }

        public static void AddUser(User user, IConfiguration configuration)
        {
            try
            {
                using (var coscineEntry = GetCoscineEntry(configuration))
                {
                    if (coscineEntry == null)
                    {
                        HandleNoCoscineEntry();
                        return;
                    }
                    try
                    {
                        // Check if user already exists in AD
                        coscineEntry.Children.Find("CN=" + user.Id);
                    }
                    catch (DirectoryServicesCOMException)
                    {
                        using (var newUser = coscineEntry.Children.Add("CN=" + user.Id, "User"))
                        {
                            SetVariableProperties(newUser, user);

                            newUser.Properties["sAMAccountName"].Value = user.Id.ToString().Substring(0, 20);
                            newUser.Properties["uid"].Value = user.Id.ToString();
                            newUser.Properties["userPrincipalName"].Value = user.Id.ToString() + "@" + configuration.GetStringAndWait("coscine/local/profilesync/forestname");

                            newUser.CommitChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleADError(e);
            }
        }

        public static void UpdateUser(User user, IConfiguration configuration)
        {
            try
            {
                using (var coscineEntry = GetCoscineEntry(configuration))
                {
                    if (coscineEntry == null)
                    {
                        HandleNoCoscineEntry();
                        return;
                    }
                    // Check if user already exists in AD
                    using (var currentUser = coscineEntry.Children.Find("CN=" + user.Id))
                    {
                        SetVariableProperties(currentUser, user);

                        currentUser.CommitChanges();
                    }
                }
            }
            catch (Exception e)
            {
                HandleADError(e);
            }
        }

        public static void DeleteUser(User user, IConfiguration configuration)
        {
            try
            {
                using (var coscineEntry = GetCoscineEntry(configuration))
                {
                    if (coscineEntry == null)
                    {
                        HandleNoCoscineEntry();
                        return;
                    }
                    // Check if user already exists in AD
                    using (var currentUser = coscineEntry.Children.Find("CN=" + user.Id))
                    {
                        currentUser.DeleteTree();
                    }
                }
            }
            catch (Exception e)
            {
                HandleADError(e);
            }
        }

        private static void HandleNoCoscineEntry()
        {
            // TODO: Implement Error Handling on no coscine entry
        }

        private static void HandleADError(Exception e)
        {
            // TODO: Implement Error Handling on error on AD connection
        }

        private static void SetVariableProperties(DirectoryEntry currentUser, User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Givenname))
            {
                currentUser.Properties["givenName"].Value = user.Givenname;
            }
            if (!string.IsNullOrWhiteSpace(user.Surname))
            {
                currentUser.Properties["sn"].Value = user.Surname;
            }
            if (!string.IsNullOrWhiteSpace(user.EmailAddress))
            {
                currentUser.Properties["mail"].Value = user.EmailAddress;
            }

            currentUser.Properties["displayName"].Value = user.DisplayName;
        }

    }
}
